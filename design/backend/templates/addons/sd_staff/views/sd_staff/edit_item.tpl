{** members section **}
{$allow_save = $member|fn_allow_save_object:"members"}
{capture name="mainbox"}
    {capture name="buttons"}
	       {if !$member.staff_id}
	           {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="members_form" but_name="dispatch[members.edit_item]"}
	       {else}
            {include file="buttons/save_cancel.tpl" but_name="dispatch[members.edit_tem]" but_role="submit-link" but_target_form="members_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$member.staff_id}
	       {/if}
	   {/capture}
	   {assign var="mail" value="e-mail"}			
	   {if !$error}
        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal form-edit cm-processed-form cm-check-changes" name="members_form">
            <input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
			         {capture name="tabsbox"}
				            <div id="content_general">
					               <input type="hidden" name="member_data[staff_id]" value="{$member.staff_id}" />
					               <div class="control-group">
						              <label for="elm_member_name" class="control-label cm-required">{__("firstname")}</label>
			                <div class="controls">
            							    <input name="member_data[name]" type="text" id="elm_member_name" value="{$member.name}">
						             </div>		
					           </div>
					           <div class="control-group">
          						    <label for="elm_member_lastname" class="control-label cm-required">{__("lastname")}</label>
						              <div class="controls">
                        <input name="member_data[lastname]" type="text" id="elm_member_lastname" value="{$member.last_name}" />
						              </div>
					           </div>
					           <div class="control-group">	
						              <label for="elm_member_email" class="control-label cm-required">{__("email")}</label>
						              <div class="controls">
							                 <input name="member_data[email]" type="text" id="elm_member_email" value="{$member.$mail}" />
						              </div>
					           </div>
					           <div class="control-group">
						              <label for="elm_member_function" class="control-label cm-required">{__("staff_function")}</label>
						              <div class="controls">
							                 <textarea name="member_data[function]" id="elm_member_function" cols="50" rows="3">{$member.function}</textarea>
	                   </div>
					           </div>
					           <div class="control-group">					
						              <label for="elm_member_photo" class="control-label cm-required">{__("photo")}</label>
          						    <div class="controls">
			                     {include file="common/attach_images.tpl"
			                         image_name="staff_image"
			                         image_object_type="staff"
				                        image_pair=$member.main_pair
			                         image_object_id=$member.staff_id
			                         image_type="M"
			                         no_detailed=true
			                         hide_titles=true
			                     }
						              </div>
					           </div>
					           <div class="control-group">
          		        <label for="elm_member_user" class="control-label cm-required">{__("user")}</label>
						              <div class="controls">
							                 <input name="member_data[user_id]" type="text" value="{$member.user_id}" />
						              </div>
					           </div>
					           <div class="control-group">
			                 <label for="elm_member_sort" class="control-label cm-required">{__("sort")}</label>
						              <div class="controls">
              						    <input name="member_data[sort]" type="text" value="{$member.sort}" />
						              </div>
						              {__("sort_description")}
					           </div>			
				        </div>
			     {/capture}
			     {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}
    </form>
    {else}
		      {__("{$error}")}
    {/if}							
{/capture}

{hook name="members:manage_mainbox_params"}
    {$page_title = __("staff_member")}
    {$select_languages = true}
{/hook}

{include file="common/mainbox.tpl"
    title=$page_title
	   content=$smarty.capture.mainbox
	   buttons=$smarty.capture.buttons
	   adv_buttons=$smarty.capture.adv_buttons
	   select_languages=$select_languages
	   sidebar=$smarty.capture.sidebar
}