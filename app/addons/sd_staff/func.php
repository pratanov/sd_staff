<?php

use Tygh\Registry;

if (!defined('AREA')) {
    die('Access denied');
}

function fn_sd_staff_get_members()
{
    $staff_members_list = db_get_hash_array("SELECT * FROM ?:sd_staff", 'staff_id');

    foreach ($staff_members_list as $key => $staff_member) {
        if ($staff_member['user_id'] > 0) {
            $additional_data = db_get_row("SELECT `firstname`, `lastname`, `email` FROM ?:users WHERE user_id = ?i", $staff_member['user_id']);
        }

        if (!empty($additional_data)) {
            $staff_members_list[$key]['name'] = $additional_data['firstname'];
            $staff_members_list[$key]['last_name'] = $additional_data['lastname'];
            $staff_members_list[$key]['e-mail'] = $additional_data['email'];
            $additional_data = false;
        }
    }

    return $staff_members_list;
}

function fn_get_members()
{
    $staff_members_list = db_get_hash_array(
            "SELECT `staff_id`, `name`, `last_name`, `function`, `user_id`, `e-mail` FROM ?:sd_staff ORDER BY `sort` ASC",
            'staff_id'
    );

    foreach ($staff_members_list as $key => $staff_member) { 
        if ($staff_member['user_id'] > 0) {
            $additional_data = db_get_row("SELECT `firstname`, `lastname` FROM ?:users WHERE user_id = ?i", $staff_member['user_id']);
        
            $staff_members_list[$key]['name'] = $additional_data['firstname'];
            $staff_members_list[$key]['last_name'] = $additional_data['lastname'];
            $additional_data = false;
        }

        $staff_members_list[$key]['main_pair'] = fn_get_image_pairs($staff_member['staff_id'], 'staff', 'M', '?:sd_staff');
    }   

    return array($staff_members_list);
}

function fn_sd_staff_get_member_mail($member_id)
{
    if (intval($member_id) > 0) {
        $member_mail = db_get_field("SELECT `e-mail` FROM ?:sd_staff WHERE staff_id = ?i", $member_id);

        if (isset($member_mail) && ($member_mail != "")) {
            return $member_mail;
        } else {
            $user_id = db_get_field("SELECT `user_id` FROM ?:sd_staff WHERE staff_id = ?i", $member_id);
            $member_mail = db_get_field("SELECT `email` FROM ?:users WHERE user_id = ?i", $user_id);
            return $member_mail;
        }
    }

    return false;
}

function fn_sd_staff_get_member($member_id)
{
    if (intval($member_id) > 0) {
        $member = db_get_row("SELECT * FROM ?:sd_staff WHERE staff_id = ?i", $member_id);
    
        return $member;
    }

    return false;
}

function fn_sd_staff_delete_member($member_id)
{
    if (intval($member_id) > 0) {
        $is_member_delete = db_query("DELETE FROM `?:sd_staff` WHERE `staff_id` = ?i", $member_id);
    
        if ($is_member_delete) {
            fn_delete_image_pairs($_REQUEST['staff_id'], 'staff', '?:sd_staff');
        }
    }

    return $is_member_delete;
}

function fn_sd_staff_update_member($member_data)
{
    $member_data['user_id'] = fn_sd_staff_user_available_check($member_data['user_id']);

    $is_member_update = db_query("UPDATE ?:sd_staff SET name = ?s, last_name = ?s, function = ?s, `e-mail` = ?s, sort = ?i, user_id = ?i WHERE staff_id = ?i;", $member_data['name'], $member_data['lastname'], $member_data['function'], $member_data['email'], $member_data['sort'], $member_data['user_id'], $member_data['staff_id']);

    fn_attach_image_pairs('staff_image', 'staff', $member_data['staff_id'], 'staff');
    
    $member = fn_sd_staff_get_member($member_data['staff_id']);
    $member['main_pair'] = fn_get_image_pairs($member['staff_id'], 'staff', 'M', '?:sd_staff');

    return $member;
}

function fn_sd_staff_add_member($member_data)
{
    $member_data['user_id'] = fn_sd_staff_user_available_check($member_data['user_id']);

    $staff_id = db_query("INSERT INTO `?:sd_staff` (`name`, `last_name`, `function`, `e-mail`, `sort`, `user_id`) VALUES (?s, ?s, ?s, ?s, ?i, ?i)", $member_data['name'], $member_data['lastname'], $member_data['function'], $member_data['email'], $member_data['sort'], $member_data['user_id']);
 
    fn_attach_image_pairs('staff_image', 'staff', $staff_id, 'staff');
 
    $member = fn_sd_staff_get_member($staff_id);
    $member['main_pair'] = fn_get_image_pairs($member['staff_id'], 'staff', 'M', '?:sd_staff');

    return $member;
}

function fn_sd_staff_user_available_check($user_id)
{
    if (intval($user_id > 0)) {
        $is_user_exist = db_get_row("SELECT * FROM ?:users WHERE `user_id` = ?i", $user_id);
        $user_id = empty($is_user_exist) ? 0 : $user_id;
    }

    return $user_id;
}
